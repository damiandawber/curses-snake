#!/usr/bin/env python

import curses
import random
import math

class World:
    """ The world in which our snake meanders """
    x = 1
    y = 3
    width = 50
    height = 15
    frames = 0
    game_over = False

    timeout = 350
    min_timeout = 20

    def __init__(self, game):
        self.game = game
        self.screen = game.screen
        self.scoreboard = Scoreboard(self.screen)
        self.draw()

    def draw(self):
        self.window = self.screen.subwin(self.height, self.width, self.y, self.x)
        self.window.border()
        self.set_timeout(self.timeout - self.scoreboard.score * 20)

        self.frames += 1
        if self.frames % 10 == 0:
            self.snake.invincible = False

        try:
            self.snake.draw()
            self.apple_collisions()
            self.apple.draw()
        except:
            return

    def apple_collisions(self):
        s = self.snake.segments[-1]
        if(s.x == self.apple.x and s.y == self.apple.y):
            self.apple.remove()
            self.create_apple()
            self.snake.grow()
            self.scoreboard.update(self.scoreboard.score + 1)

    def add_snake(self, snake):
        self.snake = snake

    def create_apple(self):
        self.apple = Apple(self, self.snake)
        self.apple.draw()

    def set_timeout(self, timeout):
        """ 
            The time between screen refreshes measured in seconds.

            We're cheating a little bit and using the screen refresh
            rate to determine the speed on the Snake object.
        """
        if(timeout < self.min_timeout): 
            timeout = self.min_timeout

        self.window.timeout(timeout)

    def lose_life(self):
        self.scoreboard.update_lives(self.scoreboard.lives - 1)
        if self.scoreboard.lives == 0:
            self.game_over = True
        self.snake.invincible = True

        self.screen.addstr(3, self.width / 2 - 5, "Aaaaaaargh", curses.color_pair(9))

class Segment:
    """ Segments are used to build up a snake """
    index = 0 
    velocity = [0,0]

    def __init__(self, world, y, x):
        self.world = world
        self.y = y
        self.x = x
    def draw(self):
        self.check_bounds()

        self.y += self.velocity[0]
        self.x += self.velocity[1]
        self.world.window.addch(self.y, self.x, "X", curses.color_pair(3))

    def check_bounds(self):
        """ 
            Check that snake segment is within world bounds and that head
            segment does not overlap with any other segment
        """
        if (self.x + self.velocity[1] == self.world.width - 1):
            self.x = 0
        elif self.x + self.velocity[1] == 0:
            self.x = self.world.width - 1
        elif self.y + self.velocity[0] == 0:
            self.y = self.world.height - 1
        elif self.y + self.velocity[0] == self.world.height - 1:
            self.y = 0

class Snake:
    """ A snake composed of segments """
    default_num_segments = 5
    segments = [] # Order is from snake tail to snake head by index
    invincible = False

    velocity = [0, 1]

    def __init__(self, world):
        self.world = world
        self.start_y = self.y = int(math.ceil(self.world.height / 2))
        self.start_x = self.x = int(math.ceil(self.world.width / 2))

        # Create body segments
        for i in range(self.default_num_segments):
            s = Segment(self.world, self.y, self.x + i)
            s.velocity = self.velocity
            s.index = i
            self.segments.append(s)

        self.draw()

    def draw(self):
        s = iter(self.segments)
        segment = s.next()
        while True:
            try:
                nexts = s.next()
                segment.velocity = nexts.velocity
                segment.draw()
                segment = nexts
            except StopIteration:
                segment.velocity = self.velocity
                segment.draw()
                break;

        if self.invincible == False:
            head = self.segments[-1]
            body_segments = [(s.y, s.x) for s in self.segments[0:-1]]
            if (head.y, head.x) in body_segments:
                self.world.lose_life()


    def grow(self):
        s = Segment(self.world, 
            self.segments[0].y - self.segments[0].velocity[0], 
            self.segments[0].x - self.segments[0].velocity[1])
        s.index = len(self.segments)
        self.segments.insert(0, s)
        s.draw()

    def set_velocity(self, v):
        self.velocity = v
        return

class Apple:
    """ Snakes eat apples """
    def __init__(self, world, snake):
        self.world = world

        random_yx = self.calculate_yx(snake)
        self.x = random_yx[1]
        self.y = random_yx[0]

    def draw(self):
        self.world.window.addch(self.y, self.x, 'O', curses.color_pair(4))

    def calculate_yx(self, snake):
        """ Available positions for apple to be drawn """
        taken_x = [s.x for s in snake.segments]
        taken_y = [s.y for s in snake.segments]
        world_x = range(1, self.world.width - 1) # Gutters are out of bounds
        world_y = range(1, self.world.height - 1)

        x = random.choice(list(set(world_x).difference(set(taken_x))))
        y = random.choice(list(set(world_y).difference(set(taken_y))))

        return y,x

    def remove(self):
        return

class Scoreboard:
    score = 0
    lives = 3

    def __init__(self,screen):
        self.screen = screen
        self.update_lives(self.lives)
        self.update(self.score)

    def update(self, score):
        self.score = score
        self.screen.move(2,1)
        self.screen.deleteln()
        self.screen.insertln()
        self.screen.addstr(2,1, "Score: %d" % score, curses.color_pair(5))

    def update_lives(self, lives):
        self.lives = lives
        self.screen.move(1,1)
        self.screen.deleteln()
        self.screen.insertln()
        self.screen.addstr(1,1, "Lives: %d" % lives, curses.color_pair(2))

class Game:
    """ Screen Initialisation """
    def __init__(self, screen):
        self.screen = screen
        self.height, self.width = self.screen.getmaxyx()
        self.screen.nodelay(0)

        curses.noecho()
        curses.cbreak()
        curses.curs_set(0)

        self.world = World(self)

    def main(self):
        snake = Snake(self.world)
        self.world.add_snake(snake)
        self.world.create_apple()

        while True:
            char = self.world.window.getch()
            if char == ord('q'):
                break
            if(self.world.game_over == False):
                if char == ord('w'):
                    snake.set_velocity([-1,0])
                elif char == ord('a'):
                    snake.set_velocity([0,-1])
                elif char == ord('s'):
                    snake.set_velocity([1,0])
                elif char == ord('d'):
                    snake.set_velocity([0,1])

                self.world.window.clear()
                self.world.draw()
                self.world.window.refresh()
                self.screen.refresh()
            else: 
                self.screen.addstr(10, self.world.width / 2 - 5, "Game Over!", curses.color_pair(6))
                self.screen.addstr(13, self.world.width / 2 - 7, "Hit q to quit.", curses.color_pair(8))
                self.screen.refresh()


# Initialise our Game using curses.wrapper()
def main(win):
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_GREEN)
    curses.init_pair(4, curses.COLOR_BLUE, curses.COLOR_BLUE)
    curses.init_pair(5, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(7, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(8, curses.COLOR_WHITE, curses.COLOR_BLACK)
    curses.init_pair(9, curses.COLOR_WHITE, curses.COLOR_RED)

    win.addstr(0,1,"Snake by Damian Dawber. w,a,s,d to move, q to quit.", curses.color_pair(1))
    win.refresh()
    game = Game(win)
    game.main()

curses.wrapper(main)

